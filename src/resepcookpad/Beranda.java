/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resepcookpad;

import java.awt.Dimension;
import java.awt.List;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;

/**
 *
 * @author Frendy Andrius
 */
public class Beranda extends javax.swing.JFrame {

    /**
     * Creates new form Beranda
     */
   
    public Beranda() {
        initComponents();
        Tampilan();
    }
    
    private void search(){
        String search = txtSearch.getText();
        try{
            String sql ="SELECT * FROM resep WHERE nama like '%"+txtSearch.getText()+"%'or deskripsi like '%"+txtSearch.getText()+"%' or resep like '%"+txtSearch.getText();
            Connection con=ModuleDB.connectDB();
            Statement stmt=con.createStatement();
            ResultSet result=stmt.executeQuery(sql);
            DefaultTableModel model=(DefaultTableModel)tblBranda.getModel();
            model.setRowCount(0);
            while(result.next()){
                Vector row=new Vector();
                row.add(result.getString("nama"));
                row.add(result.getString("deskripsi"));
                row.add(result.getString("resep"));
                row.add(result.getString("langkah"));
                model.addRow(row);
            }
            con.close();
            tblBranda.setModel(model);
        }catch(Exception e){
            
        }
    }
    
    private void filter(){
        if(Cb1.isSelected()){
            Cb2.setEnabled(false);
            Cb3.setEnabled(false);
            Cb4.setEnabled(false);
            DefaultTableModel model =new DefaultTableModel(new String[]{"Kode_Makanan","nama","deskripsi","resep","langkah"},0);  
            String sql="SELECT * FROM resep WHERE kategori LIKE 'Indonesian'";
            try{
               Connection con=ModuleDB.connectDB();
               Statement stmt = con.createStatement();
                
               ResultSet result = stmt.executeQuery(sql);
               while(result.next()){
                String kode = result.getString("Kode_Makanan");
                String nama=result.getString("nama");
                String deskripsi=result.getString("deskripsi");
                String resep=result.getString("resep");
                String langkah=result.getString("langkah");
                model.addRow(new Object[]{kode,nama,deskripsi,resep,langkah,null});
                }
            }catch(Exception e) {
             
            }  
        
        tblBranda.setModel(model);
        }
        else if(Cb2.isSelected()){
            Cb1.setEnabled(false);
            Cb3.setEnabled(false);
            Cb4.setEnabled(false);
            DefaultTableModel model =new DefaultTableModel(new String[]{"Kode_Makanan","nama","deskripsi","resep","langkah"},0);  
            String sql="SELECT * FROM resep WHERE kategori LIKE 'Western'";
            try{
               Connection con=ModuleDB.connectDB();
               Statement stmt = con.createStatement();
                
               ResultSet result = stmt.executeQuery(sql);
               while(result.next()){
                String kode = result.getString("Kode_Makanan");
                String nama=result.getString("nama");
                String deskripsi=result.getString("deskripsi");
                String resep=result.getString("resep");
                String langkah=result.getString("langkah");
                model.addRow(new Object[]{kode,nama,deskripsi,resep,langkah,null});
                }
            }catch(Exception e) {
             
            }  
        
            tblBranda.setModel(model);
            
        }
        else if(Cb3.isSelected()){
            Cb1.setEnabled(false);
            Cb2.setEnabled(false);
            Cb4.setEnabled(false);
            DefaultTableModel model =new DefaultTableModel(new String[]{"Kode_Makanan","nama","deskripsi","resep","langkah"},0);  
            String sql="SELECT * FROM resep WHERE kategori LIKE 'Japanese'";
            try{
               Connection con=ModuleDB.connectDB();
               Statement stmt = con.createStatement();
                
               ResultSet result = stmt.executeQuery(sql);
               while(result.next()){
                String kode = result.getString("Kode_Makanan");
                String nama=result.getString("nama");
                String deskripsi=result.getString("deskripsi");
                String resep=result.getString("resep");
                String langkah=result.getString("langkah");
                model.addRow(new Object[]{kode,nama,deskripsi,resep,langkah,null});
                }
            }catch(Exception e) {
             
            }  
        
        tblBranda.setModel(model);
        }
        else if(Cb4.isSelected()){
            Cb1.setEnabled(false);
            Cb2.setEnabled(false);
            Cb3.setEnabled(false);
            DefaultTableModel model =new DefaultTableModel(new String[]{"Kode_Makanan","nama","deskripsi","resep","langkah"},0);  
            String sql="SELECT * FROM resep WHERE kategori LIKE 'Chinese'";
            try{
               Connection con=ModuleDB.connectDB();
               Statement stmt = con.createStatement();
                
               ResultSet result = stmt.executeQuery(sql);
               while(result.next()){
                String kode = result.getString("Kode_Makanan");
                String nama=result.getString("nama");
                String deskripsi=result.getString("deskripsi");
                String resep=result.getString("resep");
                String langkah=result.getString("langkah");
                model.addRow(new Object[]{kode,nama,deskripsi,resep,langkah,null});
                }
            }catch(Exception e) {
             
            }  
        
        tblBranda.setModel(model);
        }
        else{
            Cb1.setEnabled(true);
            Cb2.setEnabled(true);
            Cb3.setEnabled(true);
            Cb4.setEnabled(true);
            Tampilan();
           
        }
    }

    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        btnLogout = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        menuPencarian = new javax.swing.JTextField();
        txtSearch = new javax.swing.JTextField();
        btnUpload = new javax.swing.JButton();
        menuItemHistory = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBranda = new javax.swing.JTable();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        cbSort = new javax.swing.JComboBox<>();
        Cb1 = new javax.swing.JCheckBox();
        Cb2 = new javax.swing.JCheckBox();
        Cb3 = new javax.swing.JCheckBox();
        Cb4 = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Beranda");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("ChroniCle");
        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        menuPencarian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                menuPencarianKeyPressed(evt);
            }
        });

        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
        });

        btnUpload.setText("Upload");
        btnUpload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUploadActionPerformed(evt);
            }
        });

        menuItemHistory.setText("History");
        menuItemHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemHistoryActionPerformed(evt);
            }
        });

        tblBranda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "id_user", "deskripsi", "Resep", "langkah", "Nama", "kodeMakanan"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblBranda);
        if (tblBranda.getColumnModel().getColumnCount() > 0) {
            tblBranda.getColumnModel().getColumn(5).setMinWidth(0);
            tblBranda.getColumnModel().getColumn(5).setPreferredWidth(0);
            tblBranda.getColumnModel().getColumn(5).setMaxWidth(0);
        }

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        cbSort.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nama - [A-Z]", "Nama - [Z-A]", "Kode - [Low]", "Kode - [High]", " " }));
        cbSort.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbSortActionPerformed(evt);
            }
        });

        Cb1.setText("Indonesian");
        Cb1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cb1ActionPerformed(evt);
            }
        });

        Cb2.setText("Western");
        Cb2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cb2ActionPerformed(evt);
            }
        });

        Cb3.setText("Chinese");
        Cb3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cb3ActionPerformed(evt);
            }
        });

        Cb4.setText("Japanese");
        Cb4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cb4ActionPerformed(evt);
            }
        });

        jLabel1.setText("Kategori");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(menuItemHistory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnUpload, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(btnDelete))
                .addGap(38, 38, 38)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Cb2, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Cb1)
                                    .addComponent(Cb3, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Cb4))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(139, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnSearch)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 531, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnLogout))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(342, 342, 342)
                    .addComponent(menuPencarian, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(429, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSearch)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLogout))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(btnUpload)
                        .addGap(31, 31, 31)
                        .addComponent(btnEdit)
                        .addGap(39, 39, 39)
                        .addComponent(menuItemHistory)
                        .addGap(29, 29, 29)
                        .addComponent(btnDelete))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(cbSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Cb1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Cb2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Cb3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Cb4)))
                .addContainerGap(186, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(226, 226, 226)
                    .addComponent(menuPencarian, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(334, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        this.dispose();
        ModuleDB.password=null;
        ModuleDB.username=null;
        new Login().setVisible(true);
        this.dispose(); // TODO add your handling code here:
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void menuPencarianKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_menuPencarianKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuPencarianKeyPressed

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER);
        else if(evt.getKeyCode()==KeyEvent.VK_DOWN)
        txtSearch.requestFocus();// TODO add your handling code here:
    }//GEN-LAST:event_txtSearchKeyPressed

    private void btnUploadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUploadActionPerformed
        Upload formUpload = new Upload();
        formUpload.setVisible(true);
        this.dispose();//Upload formTambahPemasukan=new Upload();
        //formTambahPemasukan.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //formTambahPemasukan.setVisible(true); // TODO add your handling code here:
    }//GEN-LAST:event_btnUploadActionPerformed

    private void menuItemHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemHistoryActionPerformed
       // History formHistory=new History();
        //formHistory.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //formHistory.setVisible(true);// TODO add your handling code here:
    }//GEN-LAST:event_menuItemHistoryActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
            String search = txtSearch.getText();
        try{
            String sql ="SELECT * FROM resep WHERE nama like '%"+txtSearch.getText()+"%'or deskripsi like '%"+txtSearch.getText()+"%' or resep like '%"+txtSearch.getText();
            Connection con=ModuleDB.connectDB();
            Statement stmt=con.createStatement();
            ResultSet result=stmt.executeQuery(sql);
            DefaultTableModel model=(DefaultTableModel)tblBranda.getModel();
            model.setRowCount(0);
            while(result.next()){
                Vector row=new Vector();
                row.add(result.getString("nama"));
                row.add(result.getString("deskripsi"));
                row.add(result.getString("resep"));
                row.add(result.getString("langkah"));
                model.addRow(row);
            }
            
            tblBranda.setModel(model);
        }catch(Exception e){
            
        }
           // TODO add your handling code here:
    }//GEN-LAST:event_btnSearchActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        this.setLocationRelativeTo(null);
        Tampilan();// TODO add your handling code here:
    }//GEN-LAST:event_formWindowOpened

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        
        int index = tblBranda.getSelectedRow();
        
        int kode = Integer.parseInt(tblBranda.getValueAt(index, 0).toString());
        
        
        EditHapus eh = new EditHapus();
        eh.setVariable(kode);
       
        eh.setVisible(true);
        this.dispose();
        
        
//        int row = tblBranda.getSelectedRow();
//        int id=0;
//        String nama;
//        if(row != -1){
//           nama = (tblBranda.getModel().getValueAt(row, 0).toString());
//           eh.editmuncul(id); 
//           eh.edit();
//        }
//        
//        Tampilan();
// TODO add your handling code here:
    }//GEN-LAST:event_btnEditActionPerformed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
                // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int row = tblBranda.getSelectedRow();
        String nama;
        if(row!=-1){
            nama=(tblBranda.getModel().getValueAt(row,0).toString());
            delete();
        }
        
        // TODO add your handling code here:
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void cbSortActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbSortActionPerformed
          int i = cbSort.getSelectedIndex();
          if(i == 0){
              DefaultTableModel model =new DefaultTableModel(new String[]{"Kode_Makanan","nama","deskripsi","resep","langkah"},0);  
              String sql="SELECT * FROM resep order by nama asc";
              try{
                Connection con=ModuleDB.connectDB();
                Statement stmt = con.createStatement();
                
                ResultSet result = stmt.executeQuery(sql);
                while(result.next()){
                    String kode = result.getString("Kode_Makanan");
                    String nama=result.getString("nama");
                    String deskripsi=result.getString("deskripsi");
                    String resep=result.getString("resep");
                    String langkah=result.getString("langkah");
                    model.addRow(new Object[]{kode,nama,deskripsi,resep,langkah,null});
                }
             }catch(Exception e) {
             
            }  
        
                tblBranda.setModel(model);
        }
          else if(i == 1){
                DefaultTableModel model =new DefaultTableModel(new String[]{"Kode_Makanan","nama","deskripsi","resep","langkah"},0);  
              String sql="SELECT * FROM resep order by nama desc";
              try{
                Connection con=ModuleDB.connectDB();
                Statement stmt = con.createStatement();
                
                ResultSet result = stmt.executeQuery(sql);
                while(result.next()){
                    String kode = result.getString("Kode_Makanan");
                    String nama=result.getString("nama");
                    String deskripsi=result.getString("deskripsi");
                    String resep=result.getString("resep");
                    String langkah=result.getString("langkah");
                    model.addRow(new Object[]{kode,nama,deskripsi,resep,langkah,null});
                }
             }catch(Exception e) {
             
            }  
        
                tblBranda.setModel(model);
            }
          else if(i == 2){
              DefaultTableModel model =new DefaultTableModel(new String[]{"Kode_Makanan","nama","deskripsi","resep","langkah"},0);  
              String sql="SELECT * FROM resep order by Kode_Makanan asc";
              try{
                Connection con=ModuleDB.connectDB();
                Statement stmt = con.createStatement();
                
                ResultSet result = stmt.executeQuery(sql);
                while(result.next()){
                    String kode = result.getString("Kode_Makanan");
                    String nama=result.getString("nama");
                    String deskripsi=result.getString("deskripsi");
                    String resep=result.getString("resep");
                    String langkah=result.getString("langkah");
                    model.addRow(new Object[]{kode,nama,deskripsi,resep,langkah,null});
                }
             }catch(Exception e) {
             
            }  
        
                tblBranda.setModel(model);
            }
          else if(i == 3){
              DefaultTableModel model =new DefaultTableModel(new String[]{"Kode_Makanan","nama","deskripsi","resep","langkah"},0);  
              String sql="SELECT * FROM resep order by Kode_Makanan desc";
              try{
                Connection con=ModuleDB.connectDB();
                Statement stmt = con.createStatement();
                
                ResultSet result = stmt.executeQuery(sql);
                while(result.next()){
                    String kode = result.getString("Kode_Makanan");
                    String nama=result.getString("nama");
                    String deskripsi=result.getString("deskripsi");
                    String resep=result.getString("resep");
                    String langkah=result.getString("langkah");
                    model.addRow(new Object[]{kode,nama,deskripsi,resep,langkah,null});
                }
             }catch(Exception e) {
             
            }  
        
                tblBranda.setModel(model);
            }
    }//GEN-LAST:event_cbSortActionPerformed

    private void Cb2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Cb2ActionPerformed
        filter();
        
   
    }//GEN-LAST:event_Cb2ActionPerformed

    private void Cb1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Cb1ActionPerformed
        filter();
        
        
        // TODO add your handling code here:
    }//GEN-LAST:event_Cb1ActionPerformed

    private void Cb3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Cb3ActionPerformed
        filter();// TODO add your handling code here:
    }//GEN-LAST:event_Cb3ActionPerformed

    private void Cb4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Cb4ActionPerformed
        filter();// TODO add your handling code here:
    }//GEN-LAST:event_Cb4ActionPerformed
     
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Beranda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Beranda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Beranda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Beranda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Beranda().setVisible(true);
            }
        });
    }
    
    
    private void delete(){
            int row=tblBranda.getSelectedRow();
            String kodemakanan=String.valueOf(tblBranda.getValueAt(row,0).toString());
            String sql="DELETE FROM resep WHERE Kode_Makanan="+kodemakanan;
            try{
                Connection con=ModuleDB.connectDB();
                Statement stmt=con.createStatement();
                stmt.executeUpdate(sql);
                showMessageDialog(null,"Data Makaanan berhasil dihapus!");
                
                new Beranda().show();
                this.dispose();
                
            }
            catch(SQLException e){
                showMessageDialog(null,e.getMessage());
            }
    }
    
    private void Tampilan(){
        
       DefaultTableModel model =new DefaultTableModel(new String[]{"Kode_Makanan","nama","deskripsi","resep","langkah"},0);  
       String sql="SELECT * FROM resep";
        try{
               Connection con=ModuleDB.connectDB();
               Statement stmt = con.createStatement();
                
               ResultSet result = stmt.executeQuery(sql);
               while(result.next()){
                String kode = result.getString("Kode_Makanan");
                String nama=result.getString("nama");
                String deskripsi=result.getString("deskripsi");
                String resep=result.getString("resep");
                String langkah=result.getString("langkah");
                model.addRow(new Object[]{kode,nama,deskripsi,resep,langkah,null});
                }
            }catch(Exception e) {
             
            }  
        
        tblBranda.setModel(model);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox Cb1;
    private javax.swing.JCheckBox Cb2;
    private javax.swing.JCheckBox Cb3;
    private javax.swing.JCheckBox Cb4;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnUpload;
    private javax.swing.JComboBox<String> cbSort;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton menuItemHistory;
    private javax.swing.JTextField menuPencarian;
    private javax.swing.JTable tblBranda;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
